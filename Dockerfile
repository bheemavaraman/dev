FROM ubuntu:latest
RUN apt-get update -y && apt-get install nodejs -y

WORKDIR /opt/

COPY lib/ /opt/lib/
COPY bin/ /var/expressCart/bin/
COPY config/ /opt/config/
COPY public/ /opt/public/
COPY routes/ /opt/routes/
COPY views/ /opt/views/
COPY locales /opt/locales/
COPY node_modules /opt/node_modules/
COPY test /opt/test/
COPY sonar-project.js /opt/
COPY app.js /opt/
COPY app.json /opt/
COPY package.json /opt/
COPY gulpfile.js /opt/
COPY jsconfig.json /opt/
COPY package-lock.json /opt/
COPY sonar-scanner-4.3.0.2102-linux /opt/expressCart/sonar-scanner-4.3.0.2102-linux/

RUN apt-get install npm -y
RUN npm install
RUN /opt/expressCart/sonar-scanner-4.3.0.2102-linux/bin/sonar-scanner \
  -Dsonar.projectKey=dev123 \
  -Dsonar.sources=. \
  -Dsonar.host.url=http://13.229.237.196:9002 \
  -Dsonar.login=9044e2b757d6593e9e73c4042a8b2dc98c6658f9
#RUN npm install 

VOLUME /opt/data
EXPOSE 1111
ENTRYPOINT ["npm", "start"]
#ENTRYPOINT ["/var/expressCart/script.sh"]
